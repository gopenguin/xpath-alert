#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import re
import json
import time
import requests
import smtplib
import argparse
import random
import UserAgent
import jsonpath_ng
import psycopg2

from jsonpath_ng import ext
from lxml import html
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime, timedelta

ua = UserAgent.UserAgent()
intervalTimeBetweenCheck = 0


class Checker:

    def __init__(self, config_path, local):
        self.config_path = config_path
        self.local = local

        self.notificator = {
            "email": self.send_email,
            "test": self.send_test
        }

        if not local:
            self.conn = psycopg2.connect(
                    database='alert',
                    user='alertmanager',
                    sslmode='require',
                    sslrootcert='/cockroach-certs/ca.crt',
                    sslkey='/cockroach-certs/client.alertmanager.key',
                    sslcert='/cockroach-certs/client.alertmanager.crt',
                    port=26257,
                    host='cockroachdb-public'
            )
            self.conn.set_session(autocommit=True)

    def send_notification(self, msg_content):
        self.notificator[self.config("sendMode")](msg_content)

    def config(self, key):
        env_value = os.environ.get(to_env_name(key))
        if env_value is not None:
            return env_value

        with open(self.config_path, 'r') as f:
            jsonpath_expr = jsonpath_ng.parse(key)
            return jsonpath_expr.find(json.loads(f.read()))[0].value

    def send_email(self, msg_content):
        try:
            # Try to login smtp server
            s = smtplib.SMTP(self.config('email.smtp'))
            s.ehlo()
            s.starttls()
            s.login(self.config('email.user'), self.config('email.password'))
        except smtplib.SMTPAuthenticationError:
            # Log in failed
            print(smtplib.SMTPAuthenticationError)
            print('[Mail]\tFailed to login')
        else:
            # Log in successfully
            print('[Mail]\tLogged in! Composing message..')

            for receiver in self.config('email.receivers'):
                msg = MIMEMultipart('alternative')
                msg['Subject'] = msg_content['Subject']
                msg['From'] = self.config('email.user')
                msg['To'] = receiver

                text = msg_content['Content']

                part = MIMEText(text, _charset='utf8')
                msg.attach(part)
                s.sendmail(self.config('email.user'), receiver, msg.as_string())
                print('[Mail]\tMessage has been sent to %s.' % receiver)

    @staticmethod
    def send_test(msg_content):
        print(msg_content)

    def get_config(self):
        if self.local:
            return [{'id': 0, 'url': '', 'type': 'jsonpath', 'pattern': '', 'match': False}]

        cur = self.conn.cursor()

        cur.execute('SELECT id, url, type, pattern, match FROM alerts')
        rows = cur.fetchall()

        configs = []

        for row in rows:
            configs.append({
                'id': row[0],
                'url': row[1],
                'type': row[2],
                'pattern': row[3],
                'match': row[4]
            })

        cur.close()

        return configs

    def update_state(self, id, match):
        if self.local:
            return True

        cur = self.conn.cursor()

        cur.execute('SELECT match FROM alerts where id = %s', (id,))
        rows = cur.fetchall()

        old_state = rows[0][0]

        if old_state == match:
            cur.close()
            return False

        cur.execute('UPDATE alerts SET match = %s WHERE id = %s', (match, id))

        cur.close()
        return True

    def get_element(self, url, type, selector):

        # set random user agent prevent banning
        r = requests.get(url, headers={
            'User-Agent':
                ua.random(),
            'Accept-Language': 'de-de',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Connection': 'keep-alive',
            'Accept-Encoding': 'gzip, deflate'
        })

        r.raise_for_status()

        if type == 'xpath':
            tree = html.fromstring(r.text)

            return tree.xpath(selector)
        elif type == 'jsonpath':
            jsonpath_expr = jsonpath_ng.ext.parse(selector)

            return jsonpath_expr.find(json.loads(r.text))


    def watch(self):
        while True:
            now_time = datetime.now()
            now_time_str = now_time.strftime('%Y-%m-%d %H:%M:%S')
            print('[%s] Start Checking' % now_time_str)

            for config in self.get_config():
                # get price and product name
                elements = self.get_element(config['url'], config['type'], config['pattern'])
    
                updated = self.update_state(config['id'], bool(elements))
    
                if updated:
                    if elements:
                        self.send_notification({
                            'Subject': 'Website Alert',
                            'Content': 'Found the following on %s\n\n%s\n\nGood Luck' % (
                                config['url'],
                                elements[0]
                            ),
                        })
                    else:
                        self.send_notification({
                            'Subject': 'Website Alert',
                            'Content': 'Sorry\n%s\nis no longer available' % (
                                config['url']
                            ),
                        })


                # time interval add some random number for preventing banning

            this_interval_time = int(self.config('interval')) + random.randint(0, int(self.config('jitter')))

            # calculate next triggered time
            dt = datetime.now() + timedelta(seconds=this_interval_time)
            print('Sleeping for %d seconds, next time start at %s\n' % (
                this_interval_time, dt.strftime('%Y-%m-%d %H:%M:%S')))
            time.sleep(this_interval_time)


def to_env_name(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    s2 = re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1)
    s3 = re.sub('([a-z0-9A-Z])[^a-z0-9A-Z]([a-z0-9A-Z])', r'\1_\2', s2)
    return 'XPATH_ALERT_' + s3.upper()


# add some arguments
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config',
                        default='%s/config.json' % os.path.dirname(os.path.realpath(__file__)),
                        help='Add your config.json path')

    parser.add_argument('--local', action='store_true', help='Don''t connect to the database')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    checker = Checker(args.config, args.local)
    checker.watch()
